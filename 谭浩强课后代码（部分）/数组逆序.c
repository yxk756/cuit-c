#include <stdio.h>
/**
 * origin   :  1 3 5 7 9
 * modified :  9 7 5 3 1
 */
int main(){
    int i,a[] = {1,3,5,7,9},temp;
    for(i=0;i<5/2;i++){
        temp = a[i];
        a[i] = a[4-i];
        a[4-i] = temp;
    }
     for(i=0;i<5;i++){
        printf("%d ",a[i]);
    }
    return 0;
}
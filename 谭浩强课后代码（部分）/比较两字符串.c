#include <stdio.h>
#include <string.h>
/**
比较两字符串 a b,
a == b 0
a > b  正数  第一个不相等的ASCII差值
a < b  负数  第一个不相等的ASCII差值
 */
int main(){
    char s1[20],s2[20];
    int i = 0,result;
    printf("Input String A：");
    scanf("%s",s1);
    getchar();
    printf("Input String B：");
    scanf("%s",s2);
    while (s1[i] == s2[i] && s2[i] != '\0') i++;
    if(s1[i] == '\0' && s2[i] == '\0')
    {
        result = 0;
    }else{
        result = s1[i] - s2[i];
    }
    
    printf("自定义比较结果:%d\n",result);
    printf("strcmp比较结果:%d\n",strcmp(s1,s2));
    return 0;
}
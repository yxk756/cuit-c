#include <stdio.h>
#include <stdlib.h>
int main(){
    int **s,i,j,n,sum = 0;
    printf("请输入维数：");
    scanf("%d",&n);
    s = (int**) malloc(sizeof(int*) * n);  //为二维数组分配行内存
    for(i=0;i<n;i++){
        s[i] = (int*)malloc(sizeof(int) * n);  //为二维数组分配列内存
    }
    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            scanf("%d",&s[i][j]);  //给数组各元素赋值
            if( i==j ){
                sum = sum + s[i][j]; //计算对角线的和
            }
        }
    }
    for(i=0;i<n;i++){  //输出
        for(j=0;j<n;j++){
           printf("%3d",s[i][j]);
        }
        printf("\n");  //换行
    }
    printf("sum = %d",sum);
    return 0;
}
#include <stdio.h>
#define N 10
struct Worker{
    char name[20]; //姓名
    int num; //工资号
} Workers[N];
//输入信息
void GetInfo(struct Worker w[N]){
    int i;
    printf("请输入%d个员工信息\n",N);
    for(i=0;i<N;i++){
        scanf("%s%d",w[i].name,&w[i].num);
    }
}
// 排序
void Sort(struct Worker w[N]){
    int i,j;
    struct Worker temp;
    for(i=0;i<N;i++){
        for(j=0;j<N-i;j++){
            if(w[j].num < w[j+1].num){
                temp = w[j];
                w[j] = w[j+1];
                w[j+1] = temp;
            }
        }
    }
}
int main(){
    int i;
    GetInfo(Workers);
    Sort(Workers);
    //输出
    for(i=0;i<N;i++){
        printf("%s--%d\n",Workers[i].name,Workers[i].num);
    }
   return 0; 
}
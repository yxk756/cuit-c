#include <stdio.h>
#include <string.h>
int main(){
    char s1[30],s2[30],i,j=0;
    gets(s1);
    for(i=0;i<strlen(s1);i++){
        if(s1[i] < '0' || s1[i] > '9'){  //如果不是数字
            s2[j] = s1[i];  //将不是数字的字符赋值给s2
            j++;//j控制s2的索引，自增1
        }
    }
    s2[++j] = '\0';  //添加新的字符结束标志
    //输出新的字符串
    printf("%s\n",s2);
    return 0;
}
#include <stdio.h>
#include <stdlib.h>
int main(){
    int a[3][4] = {2,3,2,3,2,5,4,2,3,4,6,5};
    int (*p)[4]; //数组指针，指向四个元素的指针
    p = a;
    //printf("%d",p[0][0]);
    /*------------------------------------*/
    int i,j;
    int **q;  //用二维指针定义二维数组(4*4)
    q = (int**)malloc(4 * sizeof(int*));  //给行分配内存
    for(i=0;i<4;i++){
        q[i] = (int*) malloc(4*sizeof(int)); //给列分配内存
    }
    //赋值
    for(i=0;i<4;i++){
        for(j=0;j<4;j++){
            q[i][j] = (i+1)*(j+1);
        }
    }
    //输出
    for(i=0;i<4;i++){
        for(j=0;j<4;j++){
            printf("%3d",q[i][j]);
        }
        printf("\n");
    }

    return 0;
}
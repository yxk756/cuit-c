#include <stdio.h>
#include <string.h>
#define N 10
#define M 20
void sort(char s[N][M],int type){
    int i,j;
    char temp[20];
    if(type == 1){ //按照字母递增
        for(i=0;i<N;i++){
        for(j=0;j<N-i;j++){
            if(strcmp(s[j],s[j+1]) > 0){
                strcpy(temp,s[j]);
                strcpy(s[j],s[j+1]);
                strcpy(s[j+1],temp);
            }
        }
    }
    }
    if(type == 0){ //按照字母递减
        for(i=0;i<N;i++){
        for(j=0;j<N-i;j++){
            if(strcmp(s[j],s[j+1]) < 0){
                strcpy(temp,s[j]);
                strcpy(s[j],s[j+1]);
                strcpy(s[j+1],temp);
            }
        }
    }
    }

    //输出
    printf("排序后的结果为：\n");
    for(i=0;i<N;i++){
        printf("%s\n",s[i]);
    }

    }
int main(){
    int i,type;
    char s[N][M];
    printf("请输入排序的方式，1为按照字母递增，0为按照字母递减\n");
    scanf("%d",&type);
    getchar();  //吸收一个回车
    for(i = 0; i<N;i++){
        scanf("%s",s[i]);
    }
    sort(s,type);
    return 0;
}